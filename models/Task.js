const mongoose = require("mongoose")

const task_schema = new mongoose.Schema({
	name: String,
	status: String
})

module.exports = mongoose.model("Task", task_schema)
