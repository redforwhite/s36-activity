const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const taskRoutes = require("./routes/taskRoutes");


mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@cluster0.qulgpmi.mongodb.net/s36-activity?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let ab = mongoose.connection 

ab.on("error", () => console.log("Something went wrong."))
ab.once("open", () => console.log("Connected to MongoDB!"))

const app = express()
const port = 3001


app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use("/tasks", taskRoutes)

app.listen(port, () => console.log(`Server running at localhost:${port}`))