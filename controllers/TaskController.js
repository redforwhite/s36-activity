const Task = require("../models/Task")

module.exports.getAll = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (request_body) => {
	let new_task = new Task({
		name: request_body.name,
		status: request_body.status
	})

	return new_task.save().then((created_task, error) => {
		if(error){
			return error 
		} 

		return created_task
	})
}

module.exports.updateTask = (task_id, new_content) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			return error 
		}

		result.status = new_content.status

		return result.save().then((updated_task, error) => {
			if(error){
				return error 
			}

			return updated_task
		})
	})
}

module.exports.getOne = (task_id) => {
	return Task.findById(task_id).then(result => {
		return result
	})
}